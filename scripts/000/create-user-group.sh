#!/usr/bin/env bash

if grep /etc/passwd -e mage-dev; then
    echo user mage-dev is already created
else
    sudo addgroup --gid 1001 mage-dev
    sudo adduser --uid 1001 --quiet --ingroup mage-dev --disabled-password --gecos "" mage-dev
    sudo usermod --password $(openssl passwd -1 -salt xyz xxsystem) mage-dev
    sudo usermod -a -G www-data mage-dev
    sudo usermod -a -G mage-dev vagrant
    sudo usermod -a -G www-data vagrant
    sudo usermod -a -G mage-dev www-data
    sudo usermod --password $(openssl passwd -1 -salt xyz xxsystem) -s /bin/bash www-data
fi

