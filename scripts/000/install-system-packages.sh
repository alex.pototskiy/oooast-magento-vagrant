#!/usr/bin/env bash

sudo apt-get install software-properties-common
if ! apt-cache pkgnames | grep -E '^php'${PHP_VERSION}'$'; then
    echo "add repositories for php installation"
    if [[ `lsb_release -rs` == "16.04" ]]; then
        sudo apt-add-repository -y ppa:ondrej/php
        sudo apt-add-repository -y ppa:ondrej/nginx-mainline
    fi
fi
sudo apt-get update
sudo apt-get install -y \
        bash-completion \
        htop \
        vim \
        zip \
        unzip \
        ntp \
        libsnappy-dev \
        jpegoptim \
        apt-transport-https \
        curl \
        git \
        jq \
        php${PHP_VERSION}-cli \
        php${PHP_VERSION}-fpm \
        php${PHP_VERSION}-common \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-json \
        php${PHP_VERSION}-ctype \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-intl \
        php${PHP_VERSION}-xsl \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-zip \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-iconv \
        php${PHP_VERSION}-soap \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-xdebug
#        php7.2-mcrypt \

sudo update-alternatives --set php /usr/bin/php${PHP_VERSION}

if ! composer; then
    cd ~
    curl -sS https://getcomposer.org/installer -o composer-setup.php
    sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    composer
fi
