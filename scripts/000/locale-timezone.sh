#!/usr/bin/env bash

locale -a | grep ru_RU.utf8 || sudo locale-gen ru_RU.utf8
sudo timedatectl set-timezone Asia/Vladivostok
