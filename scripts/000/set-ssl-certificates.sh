#!/usr/bin/env bash

echo "Copy ssl vagrant certificate"
if [[ ! -e /etc/ssl/certs/ssl-vagrant-server.crt ]]; then
    sudo cp /tmp/ssl-vagrant-server.crt /etc/ssl/certs
    sudo chown www-data /etc/ssl/certs/ssl-vagrant-server.crt
    sudo chmod a=rwx /etc/ssl/certs/ssl-vagrant-server.crt
    rm /tmp/ssl-vagrant-server.crt
fi
echo "Copy ssl vagrant key"
if [[ ! -e /etc/ssl/private/ssl-vagrant-server.key ]]; then
    sudo cp /tmp/ssl-vagrant-server.key /etc/ssl/private
    sudo chown www-data /etc/ssl/private/ssl-vagrant-server.key
    sudo chmod o=r /etc/ssl/private/ssl-vagrant-server.key
    rm /tmp/ssl-vagrant-server.key
fi
