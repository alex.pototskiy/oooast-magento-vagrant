#!/usr/bin/env bash

echo OOOAST: Set PHP configuration

PHPINIROOT=/etc/php/${PHP_VERSION}/

MEMORY_LIMIT=3G

for PHPINI in $(sudo find ${PHPINIROOT} -name php.ini); do
    sudo grep -q 'date.timezone = Asia/Vladivostok' ${PHPINI} &&  \
        echo php.ini timezone is already configured || \
        sudo sed -i.timezone.bak -e 's/^.*date.timezone =.*$/date.timezone = Asia\/Vladivostok/' ${PHPINI}
    sudo grep -q 'memory_limit = '${MEMORY_LIMIT} ${PHPINI} && \
        echo php.ini memory limit already configured || \
        sudo sed -i.memory_limit.bak -e 's/^.*memory_limit =.*$/memory_limit = '${MEMORY_LIMIT}'/' ${PHPINI}
done

XDEBUG_CONF=$'\"xdebug.remote_enable = 1\nxdebug.remote_connect_back = 1\nxdebug.remote_port = 9000\"'
sudo bash -c "cat >>${PHPINIROOT}mods-available/xdebug.ini <<<${XDEBUG_CONF}"

PHPWWWCONF=$PHPINIROOT/fpm/pool.d/www.conf

sudo grep -qE 'pm.max_children *= *10' ${PHPWWWCONF} && \
    echo fpm max children is already configured || \
    sudo sed -i.bak -e 's/^.*pm.max_children *=.*$/pm.max_children = 10/' ${PHPWWWCONF}
sudo grep -qE 'pm.start_servers *= *4' ${PHPWWWCONF} && \
    echo fpm max children is already configured || \
    sudo sed -i.bak -e 's/^.*pm.start_servers *=.*$/pm.start_servers = 4/' ${PHPWWWCONF}
sudo grep -qE 'pm.min_spare_servers *= *3' ${PHPWWWCONF} && \
    echo fpm max children is already configured || \
    sudo sed -i.bak -e 's/^.*pm.min_spare_servers *=.*$/pm.min_spare_servers = 3/' ${PHPWWWCONF}
sudo grep -qE 'pm.max_spare_servers *= *8' ${PHPWWWCONF} && \
    echo fpm max children is already configured || \
    sudo sed -i.bak -e 's/^.*pm.max_spare_servers *=.*$/pm.max_spare_servers = 8/' ${PHPWWWCONF}

sudo phpdismod -v ALL -s ALL xdebug
