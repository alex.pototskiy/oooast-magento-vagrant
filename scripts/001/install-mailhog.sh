#!/usr/bin/env bash

sudo apt-get install -y golang-go
if [[ ! -d ~/gocode ]]; then mkdir gocode; fi
#echo "export GOPATH=$HOME/gocode" >> ~/.profile
#source ~/.profile
if ! grep /etc/environment -e "GOPATH"; then
    sudo bash -c 'cat >> /etc/environment <<< "GOPATH=/home/vagrant/gocode"'
    source /etc/environment
fi
printenv GOPATH
GOPATH=/home/vagrant/gocode go get github.com/mailhog/MailHog
GOPATH=/home/vagrant/gocode go get github.com/mailhog/mhsendmail
sudo cp /home/vagrant/gocode/bin/MailHog /usr/local/bin/mailhog
sudo cp /home/vagrant/gocode/bin/mhsendmail /usr/local/bin/mhsendmail
if [[ ! -e /etc/systemd/system/mailhog.service ]]; then
cat > /tmp/mailhog.service <<EOL
[Unit]
Description=MailHog service

[Service]
ExecStart=/usr/local/bin/mailhog   -api-bind-addr 127.0.0.1:8025   -ui-bind-addr 127.0.0.1:8025   -smtp-bind-addr 127.0.0.1:1025

[Install]
WantedBy=multi-user.target
EOL
    sudo cp /tmp/mailhog.service /etc/systemd/system/mailhog.service
    sudo systemctl daemon-reload
fi

sudo systemctl start mailhog
sudo systemctl enable mailhog

PHPINIROOT=/etc/php/${PHP_VERSION}/

for PHPINI in $(sudo find ${PHPINIROOT} -name php.ini); do
    sudo grep -q 'sendmail_path = /usr/local/bin/mhsendmail' ${PHPINI} && \
        echo php.ini sendmail_path is already configured || \
        sudo sed -i.timezone.bak -e 's/^.*sendmail_path =.*$/sendmail_path = \/usr\/local\/bin\/mhsendmail/' ${PHPINI}
done
