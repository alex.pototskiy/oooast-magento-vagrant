#!/usr/bin/env bash

echo OOOAST: Create magento user and DB

mysql -u root -proot -h alex-winpc -e "CREATE DATABASE IF NOT EXISTS magento"
mysql -u root -proot -h alex-winpc -e "CREATE USER IF NOT EXISTS magento@'vagrant' IDENTIFIED BY 'magento'"
mysql -u root -proot -h alex-winpc -e "GRANT ALL ON magento.* TO magento@'vagrant' IDENTIFIED BY 'magento'"
mysql -u root -proot -h alex-winpc -e "flush privileges"

# sudo systemctl restart mysql
