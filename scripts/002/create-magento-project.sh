#!/usr/bin/env bash

sudo -u www-data rm -Rf /var/www/oooast
sudo mkdir -p /var/www/oooast
sudo chmod a+rw /var/www
sudo chown www-data:www-data /var/www/oooast

echo OOOAST: Create magento project
if [[ ! -e /var/www/oooast/vendor ]]; then
    cd /var/www
    sudo -H -u www-data composer config --global http-basic.repo.magento.com 6091951c2ee10870fd0374595a6d2142 4b7a340dc30fa5ed5587ed50b8310ee6
    sudo -H -u www-data composer create-project -n --no-progress --repository=https://repo.magento.com/ magento/project-community-edition:2.3.* --stability=beta oooast
    cd oooast
    sudo -H -u www-data find var generated vendor pub/static pub/media app/etc -type f -exec chmod ug+w {} +
    sudo -H -u www-data find var generated vendor pub/static pub/media app/etc -type d -exec chmod ug+w {} +
    sudo -H -u www-data chmod ug+x bin/magento
fi
