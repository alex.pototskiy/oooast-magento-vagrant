#!/usr/bin/env bash
set -x

cd /var/www/oooast

MAGE_DB_STATUS=$(sudo -H -u www-data /var/www/oooast/bin/magento setup:db:status)

echo OOOAST:MAGE INSTALL: Magento setup status: ${MAGE_DB_STATUS}
if [[ ! ${MAGE_DB_STATUS} = "All modules are up to date." ]]; then
    echo OOOAST:MAGE INSTALL: bin/magento setup:install
    sudo -H -u www-data bin/magento setup:install \
        --admin-firstname=Alexander \
        --admin-lastname=Pototskiy \
        --admin-email=alex.pototskiy.family@gmail.com \
        --admin-user=admin \
        --admin-password=xxsystem-1 \
        --base-url=http://oooast.test/ \
        --backend-frontname=myadmin \
        --db-host="alex-winpc" \
        --db-name=magento \
        --db-user=magento \
        --db-password=magento \
        --language=ru_RU \
        --currency=RUB \
        --timezone=Asia/Vladivostok \
        --use-rewrites=0 \
        --use-secure=1 \
        --base-url-secure=https://oooast.test/ \
        --use-secure-admin=1 \
        --session-save=files

    sudo -H -u www-data bin/magento config:set web/unsecure/base_url http://oooast.test/
    sudo -H -u www-data bin/magento config:set web/secure/base_url https://oooast.test/
    sudo -H -u www-data bin/magento deploy:mode:set developer --skip-compilation
    sudo -H -u www-data cp /var/www/.composer/auth.json .
    cd /var/www/oooast
    sudo -H -u www-data bin/magento cache:clean
fi
