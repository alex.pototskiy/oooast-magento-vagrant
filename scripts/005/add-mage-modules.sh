#!/usr/bin/env bash

MOD_TO_INSTALL=( \
    etws/magento-language-ru_ru \
)
#    mageplaza/module-google-recaptcha \

cd /var/www/oooast

for MOD in ${MOD_TO_INSTALL[@]}; do
    if ! sudo -H -u www-data composer info ${MOD} >/dev/null 2>/dev/null; then
        sudo -H -u www-data composer require -n --no-update ${MOD}
    fi
done

sudo -H -u www-data composer config github-oauth.github.com bcf72080d764ae4b6065cbf10cc9df4f06480bc5
sudo -H -u www-data composer config repositories.mage2-twig vcs https://github.com/SchumacherFM/Magento2-Twig.git
sudo -H -u www-data composer require -n --no-update schumacherfm/magento-twig:dev-master

sudo -H -u www-data composer update
sudo -H -u www-data bin/magento setup:upgrade
sudo -H -u www-data bin/magento setup:di:compile

sudo -H -u www-data bin/magento cache:clean

