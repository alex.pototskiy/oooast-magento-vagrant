#!/usr/bin/env bash

echo OOOAST: Create Nginx virtual host

# Recreate nginx site configuration for oooast

sudo systemctl stop nginx

[[ -e /etc/nginx/sites-enabled/oooast.conf ]] && sudo rm /etc/nginx/sites-enabled/oooast.conf
[[ -e /etc/nginx/sites-vailable/oooast.conf ]] && sudo rm /etc/nginx/sites-available/oooast.conf

sudo cp -f /vagrant/files/nginx/oooast-no-varnish.conf /etc/nginx/sites-available/oooast.conf
sudo ln -s /etc/nginx/sites-available/oooast.conf /etc/nginx/sites-enabled/oooast.conf

sudo systemctl restart nginx

