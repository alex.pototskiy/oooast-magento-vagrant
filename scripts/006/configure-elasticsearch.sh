#!/usr/bin/env bash

echo OOOAST: Install Elasticsearch plugins

#cd /usr/share/elasticsearch
#sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-phonetic
#sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-python
#sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-icu
#if sudo fgrep 'script.inline' /etc/elasticsearch/elasticsearch.yml; then
#    sudo bash -c "echo 'script.inline: on' >>/etc/elasticsearch/elasticsearch.yml"
#fi
#if sudo fgrep 'script.indexed' /etc/elasticsearch/elasticsearch.yml; then
#    sudo bash -c "echo 'script.indexed: on' >>/etc/elasticsearch/elasticsearch.yml"
#fi
#
#sudo systemctl restart elasticsearch
#
#sudo sed -i.timezone.bak -e 's/^-Xms.*$/-Xms800m/' /etc/elasticsearch/jvm.options
#sudo sed -i.timezone.bak -e 's/^-Xmx.*$/-Xmx800m/' /etc/elasticsearch/jvm.options
#sudo systemctl restart elasticsearch

cd /var/www/oooast
sudo -H -u www-data bin/magento module:disable -c Magento_Elasticsearch Magento_InventoryElasticsearch Magento_Elasticsearch6
sudo -H -u www-data composer require smile/elasticsuite:^2.7.0
sudo -H -u www-data composer require smile/module-elasticsuite-cms-search
sudo -H -u www-data composer require smile/module-elasticsuite-rating
sudo -u www-data bin/magento module:enable \
    Smile_ElasticsuiteCore \
    Smile_ElasticsuiteCatalog \
    Smile_ElasticsuiteSwatches \
    Smile_ElasticsuiteCatalogRule \
    Smile_ElasticsuiteVirtualCategory \
    Smile_ElasticsuiteThesaurus \
    Smile_ElasticsuiteCatalogOptimizer \
    Smile_ElasticsuiteTracker \
    Smile_ElasticsuiteCms \
    Smile_ElasticsuiteRating

sudo -u www-data bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/servers alex-winpc:9200
sudo -u www-data bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/enable_https_mode 0
sudo -u www-data bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/enable_http_auth 0
sudo -u www-data bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/http_auth_user ""
sudo -u www-data bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/http_auth_pwd ""
sudo -u www-data bin/magento app:config:import

sudo -u www-data bin/magento setup:upgrade

sudo -u www-data bin/magento index:reindex
