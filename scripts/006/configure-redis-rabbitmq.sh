#!/usr/bin/env bash

cd /var/www/oooast

sudo -H -u www-data bin/magento -n setup:config:set \
    --cache-backend=redis \
    --cache-backend-redis-server=127.0.0.1 \
    --cache-backend-redis-db=0 \
    --session-save=redis \
    --session-save-redis-host=127.0.0.1 \
    --session-save-redis-db=1 \
    --session-save-redis-persistent-id=sess-db \
    --page-cache=redis \
    --page-cache-redis-server=127.0.0.1 \
    --page-cache-redis-db=2 \
    --page-cache-redis-compress-data=gzip \
    --page-cache-id-prefix=fpc_ \
    --amqp-host=alex-winpc \
    --amqp-port=5672 \
    --amqp-user=magento \
    --amqp-password=magento \
    --amqp-virtualhost=/ \
    --amqp-ssl=false

echo redis set

sudo -H -u www-data bin/magento cache:flush
