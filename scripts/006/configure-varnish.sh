#!/usr/bin/env bash

sudo sed -i.bak \
    -e 's/listen 80;/listen 127.0.0.1:8080;/' \
    /etc/nginx/sites-available/oooast.conf

sudo rm /etc/nginx/sites-enabled/default

sudo systemctl restart nginx
sudo netstat -nap | grep nginx

if [[ `lsb_release -rs` == "16.04" ]]; then
    curl -s https://packagecloud.io/install/repositories/varnishcache/varnish52/script.deb.sh | sudo bash
    sudo apt-get update
fi
sudo apt-get install -y varnish
varnishd -V

if [[ ! -e /etc/systemd/system/varnish.service.d ]]; then
    sudo mkdir /etc/systemd/system/varnish.service.d
fi

cat > /tmp/varnish-override.conf <<EOL
[Service]
ExecStart=
ExecStart=/usr/sbin/varnishd -F -a 127.0.0.1:80 -T localhost:6082 -f /etc/varnish/default.vcl -S /etc/varnish/secret -s malloc,256m -p first_byte_timeout=600
EOL
sudo mv -f /tmp/varnish-override.conf /etc/systemd/system/varnish.service.d/override.conf

sudo systemctl daemon-reload
sudo systemctl restart varnish
sudo systemctl --no-pager -l status varnish

cd /var/www/oooast

sudo -u www-data bin/magento config:set --scope=default --scope-code=0 system/full_page_cache/caching_application 2
sudo -u www-data bin/magento setup:config:set --http-cache-hosts=127.0.0.1
sudo -u www-data bin/magento varnish:vcl:generate --export-version=5 > var/varnish.vcl
sudo chmod g+w var/varnish.vcl
sudo -H -u www-data sed -i.bak \
    -e 's/.url = \"\/pub\/health_check.php\";/.url = \"\/health_check.php\";/' \
    -e 's/\/pub\/health_check.php/\/\(pub\/\)\?health_check.php/' \
    var/varnish.vcl

sudo rm /etc/varnish/default.vcl
sudo ln -s /var/www/oooast/var/varnish.vcl /etc/varnish/default.vcl
sudo systemctl restart varnish nginx
sudo systemctl --no-pager -l status varnish nginx
