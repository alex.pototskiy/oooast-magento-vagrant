#!/usr/bin/env bash
set -x
cd /var/www/oooast
sudo -u www-data bin/magento setup:upgrade
sudo -u www-data bin/magento cache:clean
sudo -u www-data bin/magento cron:install --force
sudo -u www-data bin/magento config:set admin/security/session_lifetime 86400