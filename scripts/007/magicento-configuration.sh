#!/usr/bin/env bash

sudo -u www-data sed -i.bak -e \
    '/^# Banned locations.*/i\ \
# Magicento2 entry, remove in production \
location ~ (magicento2/eval_.*)\.php$ { \
    try_files \$uri =404; \
    fastcgi_pass   fastcgi_backend; \
    fastcgi_buffers 1024 4k; \
    fastcgi_param  PHP_FLAG  "session.auto_start=off \\n suhosin.session.cryptua=off"; \
    fastcgi_param  PHP_VALUE "memory_limit=756M \\n max_execution_time=18000"; \
    fastcgi_read_timeout 600s; \
    fastcgi_connect_timeout 600s; \
    fastcgi_index  index.php; \
    fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name; \
    include        fastcgi_params; \
}' /var/www/oooast/nginx.conf.sample

sudo systemctl restart nginx