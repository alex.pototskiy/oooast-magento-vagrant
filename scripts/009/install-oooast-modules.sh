#!/usr/bin/env bash

cd /var/www/oooast

sudo -H -u www-data composer config repositories.oooast-core gitlab https://gitlab.com/alex.pototskiy/oooast-magento-core.git
sudo -H -u www-data composer config repositories.oooast-tools gitlab https://gitlab.com/alex.pototskiy/oooast-magento-tools.git
sudo -H -u www-data composer require \
    oooast/module-core:dev-master \
    oooast/module-tools:dev-master

sudo -u www-data bin/magento module:enable OooAst_Core
sudo -u www-data bin/magento module:enable OooAst_Tools
sudo -u www-data bin/magento setup:upgrade
