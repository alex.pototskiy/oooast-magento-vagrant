#!/usr/bin/env bash
bold='\033[1;1m'
normal=$(tput sgr0)
RED='\033[0;31m'
NC='\033[0m'

# GLOBALS
export PHP_VERSION=7.2

if [[ ! -e ~/provision.state ]]; then
    mkdir -p ~/provision.state
fi

for step in `find /vagrant/scripts/ -type d | grep -o -e '[0-9]*' | sort`; do
    echo PROVISIONING: step ${step} started
    for script in /vagrant/scripts/${step}/*.sh; do
        if [[ -e ~/provision.state/$(basename ${script}).done ]]; then
            executed=$(stat -c %Y ~/provision.state/$(basename ${script}).done)
            modified=$(stat -c %Y ${script})
            if [[ ${executed} -gt ${modified} ]]; then
                echo PROVISIONING: script ${script} skipped
                continue
            fi
        fi
        echo PROVISIONING: script ${script} started
        bash ${script} && {
            touch ~/provision.state/$(basename ${script}).done
            echo PROVISIONING: script ${script} finished
        } || exit 1
    done
    printf PROVISIONING: step ${step} finished

done
